# Retrieval Based chatbot Agent

## 1. Design of the chatbot agent
My designed agent mainly include four components to produce a reply to a user’s question. As the following figure shows, the first component is the corpus preprocessing unit on the provided FAQ data set. It is basically a natural language processing stage to tokenize each faq sentence and remove the stopping words. The processed faq sentences are then processed in the second component, a knowledge representation unit to learn the knowledge in the questions of FAQ data set. This unit find the stem and synonym of each token word for each tokenzied faq sentence. The third component is a scoring engine to evaluate the sentence similarity between the user’s input question and each of the questions in FAQ, where the answer associated with the highest similarity score is chosen as the candidate reply. Then, its score is compared with a weight associated to the answer and decide the final reply to provide. After the user provide a feedback to the chatbot’s reply,  a weight update unit according to the feedback is processed in the fourth unit.
The corpus pre-processing and knowledge representation components are conducted before the the agent see the user’s question. Once the user input a question, the agent then find its similarity to each of the question in FAQ data sets and choose the one with the highest similarity score as the candidate reply. The obtained score is then compared the associate weight to each question in FAQ to justify if the agent is confidence enough to produce the reply. If the score is higher than its associated weight, the associated answer will be output as the reply, otherwise the agent will output “I don’t know.” In case that the agent outputs a reply, the user is required to proved a yes/no feedback, according which the agent’s weight update unit will update the associated weight for the future round justification.

![Chatbot Design](chatbot_structure.jpg "Design of Chatbot Agent")

## 2. Tools and packages used
The package I used for this project is NLTK. The output of pip freeze is listed below:

certifi==2017.7.27.1
nltk==3.2.5
numpy==1.13.3
six==1.11.0

|NLTK Function       | Usage           |
| ------------- | :------------- |
| Corpus.stopwords      |Get the set of stopping words and remove them from sentence. This function is used in the corpus preprocessing component of the agent. |
| Tokenize.word_tokenize      |Tokenize the sentence in to a list of words. This function is used to corpus preprocessing component of the agent.  |
| Stem.snowball | Get the stem of each word from the tokenized sentence. This function is used in the knowledge representation component to learn the stem of each word in the faq sentences, which will be used in the sentence similarity scoring engine to compute the word similarity and then to calculate the sentence similarity.     |
|Corpus.wordnet | Get the synonyms of each word from the tokenized sentence. This function is used in the knowledge representation component of the agent, as well as the sentence similarity scoring engine to compute the word similarity and hence the sentence similarity.|
|POS_tag |Get the POS tag of each word from the tokenized sentence. This function is used in the sentence similarity scoring engine of the agent to compute the word similarity and hence the sentence similarity. |
