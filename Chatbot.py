"""
Chatbot class - Updated 11/05/2017
This is the entry point and exit point for your chatbot.
Do not change this API. If it it changes your chatbot will
not be compatible with the autograder.

https://www.python.org/dev/peps/pep-0008/
function_names and variable:
lowercase with words separated by underscores as necessary to improve readability.
Class names should normally use the CapWords convention (yea!).
"Private" viariables start with _
Spaces are the preferred indentation method.
80 characters per line - Is this the 1980's?
I have not used a line printer since college

I highly recommend just calling your code from this file
(put your chatbot code in another file) in case we need to
change this file during the project.
"""
import nltk
import nltk.corpus
import string
import numpy as np
from nltk.corpus import wordnet
from nltk import pos_tag
# import nltk.tokenize.punkt
# import string.punctuation as punctuation

# from nltk.tokenize import PunktWordTokenizer
# punkt_tokenizer = PunktWordTokenizer()
import nltk.stem.snowball
stemmer = nltk.stem.snowball.SnowballStemmer('english')

stopwords = set(nltk.corpus.stopwords.words('english'))
stopwords.add(u'')
stopwords.add(u'abl')
www = {u'what',
 u'when',
 u'where',
 u'which',
 u'while',
 u'who',
 u'whom',
 u'why',
 u'before',
 u'after',
 u'how',
 u'not'
 }

stopwords = stopwords-www
stopwords

np.random.seed(999)


class Chatbot:

    def __init__(self, faq_path_filename):
        # faq_path_filename is string containing
        # path and filename to text corpus in FAQ format.
        # Note: You MUST use encoding="utf-8" to properly decode the FAQ
        self.faq_path_filename = faq_path_filename

        # with open(faq_path_filename, "r", encoding="utf-8") as f:  # Example code
        #     self.faq_as_list = f.readlines()                       # Example code
        # TODO: Open FAQ using encoding="utf-8" and parse question,answers
        #       into knowledge base.

        self.QDomain, self.ADomain = [],[]
        with open(self.faq_path_filename,"r", encoding="utf-8") as f: # Example code
            # self.FAQasList = f.readlines()                     # Example code
        # TODO: Open FAQ and parse question,answers
        #       into knowledge base.
            for line in f:
                question, answer = line.split("?")
                # self.QDomain.append([token.lower().strip(string.punctuation) for token in nltk.word_tokenize(question) 
                #                if token.lower().strip(string.punctuation) not in stopwords])
                # self.ADomain.append(answer)
                tokened = self.pre_process(question)
                self.QDomain.append(tokened)
                self.ADomain.append(answer)
        self.mini_Tscore = np.zeros(len(self.ADomain))
        self.max_Fscore = np.ones(len(self.ADomain))
        self.ans_score = 0
        self.ans_idx = 0
        # self.response = ""
        self.q_curr = ""
        self.a_curr = ""
        self.in_Domain = True
        # return


    def pre_process(self, line):
        qtoken = [token.lower().strip(string.punctuation) for token in nltk.word_tokenize(line) \
                    if token.lower().strip(string.punctuation) not in stopwords]
        if 'what' in qtoken and 'date' in qtoken:
            qtoken.append(u'when')
        if 'when' in qtoken:
            qtoken.append(u'what')
            qtoken.append(u'date')
        if 'location' in qtoken:
            qtoken.append(u'where')
        if 'where' in qtoken:
            qtoken.append(u'location')

        qtoken_stem = [stemmer.stem(word) for word in qtoken]
        
        return qtoken_stem    

    def word_similarity(self, word1, word2):
        sys1 = wordnet.synsets(word1)
        sys2 = wordnet.synsets(word2)

        max_score = 0
        for synset1 in sys1:
            for synset2 in sys2:
                #wup similarity
                score = synset1.wup_similarity(synset2)
                if score is not None:
                    max_score = max(score, max_score)

        return max_score

    def best_match_word(self, word, joined_set):
        max_score = 0
        # best_match = ""
        for w in joined_set:
            score = self.word_similarity(word, w)
            if max_score<score:
                # best_match=w
                max_score = score
        return max_score

    def sent2vec(self, token_list, joined_set):
        vector = np.zeros(len(joined_set))
        for i, w in enumerate(joined_set):
            if w in token_list:
                vector[i]=1
            else:
                score= self.best_match_word(w, set(token_list))
                if score > 0.25:
                    vector[i] = 1
        return vector


    def semantic_similarity(self, qtoken, intoken):
        union = set(qtoken).union(set(intoken))
        vec1 = self.sent2vec(qtoken, union)
        vec2 = self.sent2vec(intoken, union)
        vec1_norm = np.linalg.norm(vec1)
        vec2_norm = np.linalg.norm(vec2)
        simi = np.dot(vec1, vec2)/(vec1_norm*vec2_norm)
        return simi



    def cosine_similarity(self, qtoken, intoken):
        # jaccard similairty
        intersection = set(qtoken).intersection(set(intoken))
        union = set(qtoken).union(set(intoken))
        # simi = len(intersection)/len(union)
        q_left = set(qtoken)-intersection
        in_left = set(intoken)-intersection

        extra = 0
        if q_left:
            tags = pos_tag(q_left)
            for i, w in enumerate(q_left):
                tag = tags[i][1]
                if tag.startswith('V') or tag.startswith('J'):
                    score = self.best_match_word(w, in_left)
                    if score > 0.85:
                        extra += 1
        # if in_left:
        #     tags = pos_tag(in_left)
        #     for i, w in enumerate(in_left):
        #         # tag = pos_tag(w)
        #         if tags[i][1].startswith('V'):
        #             score = self.best_match_word(w, q_left)
        #             if score > 0.5:
        #                 extra += 1
        # cosine
        simi = (len(intersection)+extra)/np.sqrt(len(set(qtoken)) * len(set(intoken)))

        return simi

    def best_match(self, msg, simi_func):
        # score_list = []
        # for q in self.QDomain:
        #     simi_score = self.similarity(q, msg)
        #     score_lsit.append(simi_score)

        scores = np.array([simi_func(q,msg) for q in self.QDomain])
        # print(scores)

        max_score = max(scores)
        index = np.argmax(scores)

        # print(index, max_score)
        return index, max_score





    # user_feedback(yesorno : boolean, correct_response : string):
    #      yesorno = True - Your previous response was correct
    #                False - Your previous response was incorrect
    #     updated_response = Response to ADD to FAQ
    def user_feedback(self, yesorno, updated_response):
        # TODO:
        # if yesorno == True, you answered the prvious question correctly
        # if yesorno == False, you answered the previous question incorrectly
        # if updated_response != "", you need to update the previous response in the FAQ
        # You WILL get feedback after EVERY question

        if updated_response:
            if (self.a_curr == "I do not know." and yesorno == True) or (yesorno == False):
                self.in_Domain = False
                self.QDomain.append(self.q_curr)
                self.ADomain.append(updated_response)
                self.idx = len(self.QDomain)-1
                self.mini_Tscore = np.append(self.mini_Tscore, 0)
                self.max_Fscore = np.append(self.max_Fscore, 1)
            else:
                self.ADomain[self.idx] = updated_response

                self.mini_Tscore[self.idx] = 0
                self.max_Fscore[self.idx] = 1


        if yesorno==True and self.ans_score <self.mini_Tscore[self.idx]:
            self.mini_Tscore[self.idx] = self.ans_score            
        elif self.ans_score > self.max_Fscore[self.idx]:
            self.max_Fscore[self.idx] = self.ans_score

            # print("Ater user feedback: ")
            # print("Appended question: " + str(self.QDomain[-1]))
            # print("Appended answer: " + str(self.ADomain[-1]))
            # print("count of QDomain :" + str(len(self.QDomain)))
            # print("count of ADomain :" + str(len(self.ADomain))+"\n")
        # print("Updating FAQ: "+updated_response)  # Example code
        # print(self.msg + ":" + str(self.ans_score) + "\n\n")
        # print(self.QDomain[self.idx])
        # return

    # input_output(msg : string) :        response : string
    #      msg          =  string from user (will not have ? at end)(no case guarantee)
    #      response = Text response from FAQ
    def input_output(self, msg):
        # TODO: Insert calls to your chatbot here
        #       Your chatbot should return '' if
        #       it does not have an answer.
        response = ''
        # for qa in self.faq_as_list:              # Example code
        #     if len(qa.split('?')) < 2: continue  # Example code
        #     question = qa.split('?')[0]          # Example code
        #     answer = qa.split('?')[1]            # Example code
        #     if question == msg:                  # Example code
        #         response = answer                # Example code
        #         break                            # Example code


        # total 23 q
        # replace 7 q


        inQ = self.pre_process(msg)

        # for i, qa in enumerate(self.QDomain):
            # if qa == inQ:
            #     response = self.ADomain[i]
        self.idx, self.ans_score = self.best_match(inQ, self.cosine_similarity)
        # if self.ans_score < 0.1:
        #     self.idx, self.ans_score = self.best_match(inQ, self.semantic_similarity)

        # response = self.ADomain[idx]
        # self.ans_score[self.idx] = score

        if self.ans_score > 0.5 and self.ans_score>=self.mini_Tscore[self.idx]:
            response = self.ADomain[self.idx]
        elif self.ans_score >= self.max_Fscore[self.idx] and self.ans_score < self.mini_Tscore[self.idx]:
            rand = np.random.random()
            if rand>0.5 and self.ans_score > 0.5:
                response = self.ADomain[self.idx]


        # You should not need to change any of the code below
        # this line.

        # If your agent does not know the answer
        if not response:
            response = "I do not know."

        # response = response + ":" + str(self.ans_score)
        # #============================
        # # expand questions domain
        # self.QDomain.append(msg)
        # # self.ADomain.append("I do not know.")
        # self.ADomain.append(response)

        # If your agent knows the answer
        self.msg = msg
        self.q_curr = inQ
        self.a_curr = response


        # print("================================")
        # print(inQ)
        # print(self.QDomain[self.idx])
        # print(self.ADomain[self.idx])
        # print("Before user feedback:")
        # print("Question: " + str(self.q_curr))
        # print("Answer: " + self.a_curr)
        # print("count of question Domain: " + str(len(self.QDomain)) + "and answer Domain: " + str(len(self.ADomain))+"\n")



        return response
